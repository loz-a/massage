require([
    'dojo/query',
    'dojo/on',
    'dojo/dom-class',
    'dojo/dom-attr'
],
function(query, on, domClass, domAttr) { "use strict";

    var readyNodes = query('.animation-ready');

    on(window, 'scroll', function(e) {

        readyNodes.forEach(function(node) {
            if (isVisible(node) && domClass.contains(node, 'animation-ready')) {

                var animName = domAttr.has(node, 'data-animation-name') ? domAttr.get(node, 'data-animation-name') : 'fadeIn';

                domClass.replace(node, 'animated ' + animName, 'animation-ready');
            }
        });

    });


    function isVisible(elem) {
        var coords = getCoords(elem);
        var windowTop = window.pageYOffset || document.documentElement.scrollTop;
        var windowBottom = windowTop + document.documentElement.clientHeight;

        coords.bottom = coords.top + elem.offsetHeight;

        // верхняя граница elem в пределах видимости ИЛИ нижняя граница видима
        var topVisible = coords.top > windowTop && coords.top < windowBottom;
        var bottomVisible = coords.bottom < windowBottom && coords.bottom > windowTop;

        return topVisible || bottomVisible;
    }


    function getCoords(elem) {
        var box = elem.getBoundingClientRect();

        var body = document.body;
        var docElem = document.documentElement;

        var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
        var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

        var clientTop = docElem.clientTop || body.clientTop || 0;
        var clientLeft = docElem.clientLeft || body.clientLeft || 0;

        var top  = box.top +  scrollTop - clientTop;
        var left = box.left + scrollLeft - clientLeft;

        return { top: Math.round(top), left: Math.round(left) };
    }



});


