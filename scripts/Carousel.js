require([
    'dojo/_base/lang',
    'dojo/query',
    'dojo/on',
    'dojo/mouse',
    'dojo/dom-class',
    'dojo/dom-attr',
    'dojo/NodeList-traverse',
    'dojo/NodeList-dom',
    'dojo/NodeList-data',
    'dojo/domReady!'
], function(
    lang,
    query,
    on,
    mouse,
    domClass,
    domAttr
) { "use strict";

    var Carousel = function(element, options) {

        if (element.tagName) {
            this.domNode  = element;
            this.$element = query(element);
        } else {
            this.domNode  = element[0];
            this.$element = element;
        }

        this.$indicators = this.$element.query('.carousel-indicators');
        this.options     = options;
        this.paused      =
        this.sliding     =
        this.interval    =
        this.$active     =
        this.$items      = null;

        if (this.options.pause === 'hover') {
            on(this.domNode, mouse.enter, lang.hitch(this, 'pause'));
            on(this.domNode, mouse.leave, lang.hitch(this, 'cycle'));
        }
    };

    Carousel.DEFAULTS = {
        interval: 4000,
        pause: 'hover',
        wrap: true
    };


    Carousel.prototype.cycle = function(e) {
        e || (this.paused = false);

        this.interval && clearInterval(this.interval);

        this.options.interval
            && !this.paused
            && (this.interval = setInterval(lang.hitch(this, 'next'), this.options.interval));

        return this;
    }; // cycle()


    Carousel.prototype.getActiveIndex = function() {
        this.$active = this.$element.query('.item.active');
        this.$items  = this.$active.parent().children();

        return this.$items.indexOf(this.$active[0]);
    }; // getActiveIndex()


    Carousel.prototype.to = function(pos) {
        var activeIndex = this.getActiveIndex();

        if (pos > (this.$items.length - 1) || pos < 0) return;

        if (this.sliding) return on.once(this, 'slid.bs.carousel', lang.hitch(this, function() {
            this.to(pos);
        }));

        if (activeIndex === pos) return this.pause().cycle();

        return this.slide(pos > activeIndex ? 'next' : 'prev', query(this.$items[pos]));
    }; // to()


    Carousel.prototype.pause = function(e) {
        e || (this.paused = true);

        if (this.$element.query('.next, .prev').length && transitionEnd.isSupport()) {
            on.emit(this.domNode, transitionEnd.eventName, {cancelable: true, bubbles: true});
            this.cycle(true);
        }
        this.interval = clearInterval(this.interval);
        return this;
    }; // pause()


    Carousel.prototype.next = function() {
        if (this.sliding) return;
        return this.slide('next');
    }; // next()


    Carousel.prototype.prev = function() {
        if (this.sliding) return;
        return this.slide('prev');
    }; // prev()


    Carousel.prototype.slide = function(type, next) {
        var $active   = this.$element.query('.item.active');
        var $next     = next || $active[type]();
        var isCycling = this.interval;
        var direction = type === 'next' ? 'left' : 'right';
        var fallback  = type === 'next' ? 'first' : 'last';

        if (!$next.length) {
            if (!this.options.wrap) return;
            $next = this.$element.query('.item')[fallback]();
        }

        this.sliding = true;

        isCycling && this.pause();

        var e = {
            relatedTarget: $next[0],
            direction: direction,
            cancelable: true,
            bubbles: true
        };

        if (domClass.contains($next[0], 'active')) return;

        if (this.$indicators.length) {
            this.$indicators.query('.active').removeClass('active');
            on.once(this.domNode, 'slid.bs.carousel', lang.hitch(this, function() {
                var $nextIndicator = query(this.$indicators.children()[this.getActiveIndex()]);
                $nextIndicator && $nextIndicator.addClass('active');
            }));
        }

        if (transitionEnd.isSupport() && domClass.contains(this.domNode, 'slide')) {
            if (!on.emit(this.domNode, 'slide.bs.carousel', e)) return;

            $next.addClass(type);
            $next[0].offsetWidth; // force reflow
            $active.addClass(direction);
            $next.addClass(direction);
            on.once($active[0], transitionEnd.eventName, lang.hitch(this, function() {
                $next.removeClass([type, direction]).addClass('active');
                $active.removeClass(['active', direction]);
                this.sliding = false;
                var that = this;
                setTimeout(function() { on.emit(that.domNode, 'slid.bs.carousel', e); }, 0);
            }));
            transitionEnd.emulate($active[0], 600);
        } else {
            if (!on.emit(this.domNode, 'slide.bs.carousel', e)) return;
            $active.removeClass('active');
            $next.addClass('active');
            this.sliding = false;
            on.emit(this.domNode, 'slid.bs.carousel', e);
        }

        isCycling && this.cycle();

        return this;
    }; // slide()


    // CAROUSEL PLUGIN DEFINITION
    // ==========================
    lang.extend(query.NodeList, {
        carousel: function(option) {
            var data    = this.data('bs.carousel')[0];
            var options = lang.mixin({}, Carousel.DEFAULTS, this.data()[0], typeof option == 'object' && option);
            var action  = typeof option === 'string' ? option : options.slide;

            if (!data) this.data('bs.carousel', (data = new Carousel(this, options)));
            if (typeof option === 'number') data.to(option);
            else if (action) data[action]();
            else if (options.interval) data.pause().cycle();
        }
    });


    // CAROUSEL DATA-API
    // =================

    on(document, on.selector('[data-slide], [data-slide-to]', 'click'), function(e) {
        var $this   = query(this), href;
        var $target = query(domAttr.get(this, 'data-target') || (href = domAttr.get(this, 'href')) && href.replace(/.*(?=#[^\s]+$)/, '')); //strip for ie7
        var options = lang.mixin({}, $target.data()[0], $this.data()[0], getNodeDataSet(this));
        var slideIndex = domAttr.get(this, 'data-slide-to');
        if (slideIndex) options.interval = false;

        $target.carousel(options);

        if (slideIndex = domAttr.get(this, 'data-slide-to')) {
            $target.data('bs.carousel')[0].to(slideIndex);
        }

        e.preventDefault();
    });

    on(window, 'load', function() {
        query('[data-ride="carousel"]').forEach(function(node) {
            query(node).carousel(getNodeDataSet(node));
        });
    });


    var transitionEnd = (function() {

        var eventName = (function() {
            var el = document.createElement('fakeelement');

            var eventNames = {
                'WebkitTransition' : 'webkitTransitionEnd',
                'MozTransition'    : 'transitionend',
                'OTransition'      : 'oTransitionEnd otransitionend',
                'transition'       : 'transitionend'
            };

            for (var name in eventNames) {
                if (el.style[name] !== undefined) return eventNames[name];
            }
            return '';
        })();

        return {
            eventName: eventName,
            isSupport: function() {
                return !!this.eventName;
            },
            emulate: function(domNode, duration) {
                var called = false;
                on.once(domNode, this.eventName, function() { called = true });

                var callback = function() {
                    if (!called) on.emit(domNode, this.eventName, {cancelable: true, bubbles: true});
                };
                setTimeout(callback, duration);
            }
        }

    })();


    function getNodeDataSet(node) {
        if (node.dataset) return lang.mixin({}, node.dataset);

        var attrs = node.attributes, dataset = {}, len = attrs.length;
        for (var i = 0; i < len; i++) {
            if (attrs[i].name.substring(0, 5) === 'data-') {
                dataset[attrs[i].name.substring(5)] = attrs[i].value;
            }
        }
        return dataset;
    } // getNodeDataSet()

});
