require([
    'dojo/query',
    'dojo/_base/config',
    'put-selector',
    'google-maps-loader'
],
function(query, config, put, gmloader) { "use strict";

    var gMapsViewer = new (function() {

        var overlay, expander, gmap, gmarker;

        function createExpander() {
            overlay  = put(document.body, 'div.overlay.hide');
            expander = put(overlay, 'div#expander');
        }

        function showExpander() {
            put(overlay, '!hide');
        }

        function hideExpander() {
            put(overlay, '.hide');
        }


        function viewMap(node) {

            var latitude, longitude, targetname;

            function parseAttributes(node) {

                latitude  = node.getAttribute('data-latitude');
                longitude = node.getAttribute('data-longitude');

                if (!latitude) { throw new Error('Latitude is undefined'); }
                if (!longitude) { throw new Error('Longitude is undefined'); }

                targetname = node.getAttribute('data-targetname') || '';
            }

            function viewGmaps(maps) {
                // http://code.google.com/intl/ru/apis/maps/documentation/javascript/basics.html#Welcome
                var latlng = new maps.LatLng(latitude, longitude);

                if (!gmap) {
                    gmap = new maps.Map(expander, {
                        zoom: 17,
                        center: latlng,
                        mapTypeId: maps.MapTypeId.HYBRID
                    });

                    if (!gmarker) {
                        gmarker = new maps.Marker({
                            position: latlng,
                            map: gmap,
                            animation: maps.Animation.BOUNCE,
                            title: targetname
                        });
                    }

                    maps.event.addListener(gmarker, 'mouseover', function() { gmarker.setAnimation(null); });
                    maps.event.addListener(gmarker, 'mouseout', function() { gmarker.setAnimation(maps.Animation.BOUNCE); });

                    var closeBtn = put(expander, 'div.close');
                    closeBtn.innerHTML = '&times;';
                    query(closeBtn).on('click', hideExpander);
                } else {
                    gmap.setCenter(latlng);
                    gmarker.setPosition(latlng);
                }

            }

            parseAttributes(node);

            var loadedMaps = gmloader.load(
                'http://maps.googleapis.com/maps/api/js',
                {
                    sensor: false,
                    language: config.locale
                }, viewGmaps);

            if (loadedMaps) { viewGmaps(loadedMaps); }
        }


        query('.find-on-map')
            .on('click', function(evt) {
                evt.preventDefault();

                if (!expander) {
                    createExpander();
                }
                showExpander();
                viewMap(this);
            });


    })();

});
