require([
    'dojo/query',
    'dojo/request',
    'dojo/Deferred',
    'dojo/promise/all',
    'dojo/dom-construct',
    'dojo/dom-class',
    'dojo/on',
//    'DomStorage',
    'Carousel',
    'navigation',
    'view-photos',
    'dojo/domReady!'
],
function(
    query,
    request,
    Deferred,
    promiseAll,
    domConstruct,
    domClass,
    on
//    ,
//    DomStorage
) { "use strict";

    var containerDOMNode = query('.main-container')[0];
    var currentPageContentDOMNode = query('.page', containerDOMNode)[0];

    var currentPage = 'index';
    if (location.pathname.indexOf('.html') >= 0) {
        currentPage = location.pathname.split('.html')[0].slice(0);
    }

    var currentPageIdx = 0;
    var requestsList = [];
//    var domStorage = DomStorage(true);

//    var lastStorageModified, lastDocumentModified;
//    if (domStorage) {
//        lastStorageModified = domStorage.read('lastModified');
//        lastDocumentModified = (new Date(document.lastModified)).getTime();
//
//        if (lastStorageModified != lastDocumentModified) {
//            domStorage.clear();
//        }
//    }


    function getPage(href) {
        var dfd = new Deferred();

        var process = (function() {
            var result = domStorage && domStorage.read(href);
            if (result) {
                dfd.resolve(result);
                return dfd.promise;
            }
            return request.get(href);
        })();


        process.then(function(result) {
            if (domStorage && !domStorage.has(href)) {
                domStorage.write(href, result);

                if (!domStorage.has('lastModified')) {
                    domStorage.write('lastModified', lastDocumentModified);
                }
            }
            return dfd.promise;
        });

        return process;
    } // getPages()


    query('nav a').forEach(function(node, idx) {
        if (node.href.indexOf(currentPage + '.html') >= 0) {
            currentPageIdx = idx;
            return;
        }
        console.log(node.href)
//        requestsList[idx] = getPage(node.href);
        requestsList[idx] = request.get(node.href);
    });

    promiseAll(requestsList)
        .then(function(results) {
            var pageDOMNode;
            for (var key in results) {
                if (results[key] == undefined || key == currentPageIdx) {
                    continue;
                }
                pageDOMNode = domConstruct.toDom(results[key]);

                if (key > currentPageIdx) {
                    containerDOMNode.appendChild(pageDOMNode);
                } else {
                    containerDOMNode.insertBefore(pageDOMNode, currentPageContentDOMNode);
                }

                require(['location']);
            }
        })
        .then(function() {
            require(['lazy-animation']);
        });

    on(
        document.getElementById('page-index'),
        on.selector('.read-more, .hide-details', 'click'), function() {
            var ddNode = (this.parentNode.nodeName.toUpperCase() == 'DD') ? this.parentNode : this.parentNode.parentNode;

            query('.read-more, .hide-details, p:first-child ~ *', ddNode)
                .forEach(function(node) {
                    domClass.toggle(node, 'hidden');
                });
        });

});