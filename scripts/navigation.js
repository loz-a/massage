require([
    'dojo/query',
    'dojo/on',
    'dojo/dom-geometry',
    'dojo/dom-class',
    'dojo/dom-style',
    'dojo/dom',
    'dojo/_base/lang',
    'dojo/domReady!'
], function(
    query,
    on,
    domGeometry,
    domClass,
    domStyle,
    dom,
    lang
) { 'use strict';

    var navNode        = query('nav')[0];
    var menuButtonNode = dom.byId('menu-btn');
    var iconNode       = query('span', menuButtonNode)[0];
    var containerNode  = dom.byId('main');
    var carouselNode   = dom.byId('head-carousel');

    var headerBottom = navNode.offsetHeight + getYCoord(navNode);
    var originalWidth = navNode.offsetWidth;

    adjustMenuButton();
    on(window, 'resize', function() {
        adjustMenuButton();
        resizeNavigation();
    });


    on(window, 'scroll', function(e) {
        var scrollY = domGeometry.docScroll().y;

        if (scrollY >= headerBottom) {
            domClass.add(menuButtonNode, 'visible');
        } else {
            domClass.remove(menuButtonNode, 'visible');
            containerNode.insertBefore(navNode, carouselNode);
            createHeaderNotFixed();
        }
    });


    on(menuButtonNode, 'click', function() {
        if (domClass.contains(navNode, 'fixed-nav')) { // is menu is visible after scroll
            containerNode.insertBefore(navNode, carouselNode);
            createHeaderNotFixed();
        } else {
            document.body.appendChild(navNode);
            createHeaderFixed();
        }
    });


    function createHeaderFixed() {
        domClass.add(navNode, 'fixed-nav');
        navNode.style.width = originalWidth + 'px';

        navNode.style.display = 'block';
        menuButtonNode.style.top = navNode.offsetHeight + 10 + 'px';
        navNode.style.display = '';

        domClass.remove(iconNode, 'icon-menu');
        domClass.add(iconNode, 'close');
    } // createHeaderFixed()


    function createHeaderNotFixed() {
        domClass.remove(navNode, 'fixed-nav');
        navNode.style.width = '';
        menuButtonNode.style.top = '10px';

        domClass.remove(iconNode, 'close');
        domClass.add(iconNode, 'icon-menu');
    } // createHeaderNotFixed()


    function resizeNavigation() {
        var computedStyle = domStyle.getComputedStyle(navNode);
        var lOffset = parseFloat(computedStyle.marginLeft);
        var rOffset = parseFloat(computedStyle.marginRight);
        var newWidth = navNode.parentNode.clientWidth - (lOffset + rOffset);

        originalWidth = newWidth;
        navNode.style.width = newWidth + 'px';

        if (domClass.contains(navNode, 'fixed-nav')) {
            navNode.style.display = 'block';
            menuButtonNode.style.top = navNode.offsetHeight + 10 + 'px';
            navNode.style.display = '';
        }
    } // resizeNavigation()


    function adjustMenuButton() {
        menuButtonNode.style.right = (document.body.clientWidth >= 1170 ? containerNode.offsetLeft : 30) + 'px';
    } // adjustMenuButton()


    function getYCoord(elem) {
        var box   = elem.getBoundingClientRect();
        var body  = document.body;
        var docEl = document.documentElement;

        var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
        var clientTop = docEl.clientTop || body.clientTop || 0;

        return box.top + scrollTop - clientTop;
    } // getYCoord()


    // SCROLL_TO_TARGET
    // =============================

    function ScrollToTarget(option) {
        var options = lang.mixin({}, ScrollToTarget.DEFAULTS, typeof option === 'object' && option);
        var step;
        var delay;
        var pageYOffset;
        var targetScrollTop;
        var currentYPos;

        var scrollTop = function() {
            var finalPos = Math.max(0, targetScrollTop);
            var timer = setInterval(function() {
                currentYPos -= step;
                window.scrollTo(0, currentYPos);

                if (currentYPos <= finalPos) {
                    clearInterval(timer);
                    if (currentYPos <= 0) {
                        createHeaderNotFixed();
                    }
                }
            }, delay);
        };

        var scrollBottom = function() {
            var finalPos = targetScrollTop - 30;

            var timer = setInterval(function() {
                currentYPos += step;
                window.scrollTo(0, currentYPos);
                (Math.min(currentYPos, finalPos) >= finalPos) && clearInterval(timer);
            }, delay);

        };

        this.scroll = function(option) {

            step = option.step || options.step;
            delay = option.delay || options.delay;
            pageYOffset = currentYPos = option.pageYOffset || options.pageYOffset;
            targetScrollTop = option.targetScrollTop;

            if (targetScrollTop === pageYOffset) {
                return;
            }

            targetScrollTop > pageYOffset ? scrollBottom() : scrollTop();
        };
    }

    ScrollToTarget.DEFAULTS = {
        step: 20,
        delay: 3,
        pageYOffset: 0
    };


    var scroller = new ScrollToTarget();

    on(document, 'nav ul a:click', function(e) {
        e.preventDefault();
        var targetId = this.getAttribute('data-target');

        if (!targetId) {
            return;
        }

        var target = dom.byId(targetId);
        var pageYOffset = domGeometry.docScroll().y;
        var targetScrollTop = domGeometry.position(dom.byId(targetId), true).y;

        scroller.scroll({
            pageYOffset: pageYOffset,
            targetScrollTop: targetScrollTop
        });

    });

});