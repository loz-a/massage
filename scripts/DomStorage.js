define([], function() {
    "use strict";

    return function(persist) {

        try {
            var storage = window[persist ? 'localStorage' : 'sessionStorage'];
            if (!storage) { return null; }
        } catch(e) {
            return null;
        }

        return {
            read: function(key) {
                return storage.getItem(key);
            },


            write: function(key, value) {
                try {
                    return storage[key] = value.toString();
                } catch(e) {
                    return null;
                }
            },


            has: function(key) {
                return !!storage.getItem(key);
            },


            erase: function(key) {
                storage.removeItem(key);
                return true;
            },


            keys: function() {
                var keys = [];
                for (var i = 0; i < storage.length; i++) {
                    keys.push(storage.key(i));
                }
                return keys;
            },


            clear: function() {
                try {
                    storage.clear();
                    return true;
                } catch(e) {
                    return false;
                }
            }
        };
    };
});