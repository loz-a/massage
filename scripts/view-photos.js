require([
    'dojo/_base/lang',
    'dojo/query',
    'put-selector',
    'dojo/on',
    'dojo/request',
    'dojo/dom',
    'dojo/dom-style',
    'dojo/domReady'
], function(
    lang,
    query,
    put,
    on,
    request,
    dom,
    domStyle,
    domReady
) { "use strict";

    var PhotosExpander = function() {

        this.overlayNode     =
        this.expanderNode    =
        this.imageViewerNode =
        this.imagesListNode  =
        this.closeBtnNode    = null;

    } // PhotosExpander


    PhotosExpander.prototype.createDOM = function() {
        this.overlayNode     = put(document.body, 'div.overlay.hide');
        this.expanderNode    = put(this.overlayNode, 'div#photo-expander');
        this.imageViewerNode = put(this.expanderNode, 'div.image-viewer');
        this.imagesListNode  = put(this.expanderNode, 'div.images-list');

        this.closeBtnNode    = put(this.expanderNode, 'div.close',
            {innerHTML: '&times;', onclick: lang.hitch(this, this.hide)});

        on(window, 'resize', lang.hitch(this, function() {
            // if (overlayNode is visible
            if (this.overlayNode.className.indexOf('hide') < 0) this.adjustImage();
        }));
    }; // createDOM()


    PhotosExpander.prototype.show = function(targetNode) {
        this.overlayNode || this.createDOM();

        put(this.overlayNode, '!hide');

        this
            .getImages(targetNode.getAttribute('href'))
            .then(lang.hitch(this, function() {
                var linksNodeList = query('li:first-child a', this.imagesListNode);
                if (linksNodeList.length) {
                    this.selectImage(linksNodeList[0].getAttribute('href'));
                }
            }));
    }; // show()


    PhotosExpander.prototype.hide = function() {
        this.overlayNode && put(this.overlayNode, '.hide');
        // delete image in viewer
        put(query('img', this.imageViewerNode)[0], '!');
        // delete images list
        put(query('ul', this.imagesListNode)[0], '!');
    }; // hide()


    PhotosExpander.prototype.selectImage = function(imageUrl) {
        var imgNode,
            that = this,
            imgNodeList = query('img', this.imageViewerNode);

        imgNode = imgNodeList.length ? imgNodeList[0] : put(this.imageViewerNode, 'img');

        imgNode.onload = function() {
            that.adjustImage(imgNode);
        };
        imgNode.src = imageUrl;
    }; // selectImage()


    PhotosExpander.prototype.adjustImage = function(imageNode) {

        if (!imageNode) {
            imageNode = query('img', this.imageViewerNode)[0];
        }

        domStyle.set(imageNode, { width: '', height: '', marginTop: '' });

        var imgWidth   = imageNode.offsetWidth,
            imgHeight  = imageNode.offsetHeight,
            avblWidth  = Math.floor(this.expanderNode.clientWidth), // availableWidth
            avblHeight = Math.floor(this.expanderNode.clientHeight - this.imagesListNode.offsetHeight), // availableHeight
            size;

        if (imgWidth >= imgHeight) {
            size = PhotosExpander.resizeImage(imgWidth, imgHeight, avblWidth, avblHeight);
        } else if (imgHeight >= imgWidth) {
            size = PhotosExpander.resizeImage(imgHeight, imgWidth, avblHeight, avblWidth);
            var height  = size.height;
            size.height = size.width;
            size.width  = height;
        }

        domStyle.set(imageNode, {
            width:  (size.width - 40) + 'px',
            height: (size.height - 40) + 'px'
        });
        var calcMarginTop = (this.expanderNode.clientHeight - (this.imageViewerNode.offsetHeight + this.imagesListNode.offsetHeight)) / 2;
        domStyle.set(imageNode, 'marginTop', (calcMarginTop > 0 ? calcMarginTop : 0) + 'px');
    }; // adjustImage()


    PhotosExpander.resizeImage = function(imageWidth, imageHeight, maxWidth, maxHeight) {
        var width  = imageWidth,
            height = imageHeight;

        if (imageWidth > maxWidth) {
            width  = maxWidth;
            height = Math.floor(width / (imageWidth / imageHeight));
        }

        if (height > maxHeight) {
            var oldHeight = height;
            height = maxHeight;
            width  = Math.floor(height / (oldHeight / width));
        }

        return {
            height: height,
            width: width
        }
    }; // resizeImage()


    PhotosExpander.prototype.getImages = function(imagesUrl) {
        put(this.expanderNode, '.preload')

        return request(imagesUrl).then(dojo.hitch(this, function(data) {
            this.imagesListNode.innerHTML = data;
            put(this.expanderNode, '!preload');
        }));

    }; // getImages()


    domReady(function() {

        var expander = new PhotosExpander();

        on(document, on.selector('.view-photos', 'click'), function(e) {
            e.preventDefault();
            expander.show(e.target);
        });

        on(document, on.selector('#photo-expander .images-list a', 'click'), function(e) {
            e.preventDefault();

            var target = (e.target.nodeName.toUpperCase() === 'IMG') ? e.target.parentNode : e.target;
            expander.selectImage(target.getAttribute('href'));
        });

    });

});