/**
 * Dojo AMD Google Maps Loader Plugin
 */
define([
    "dojo/_base/kernel",
    "dojo/topic",
    "dojo/io-query"
], function(kernel, topic, ioQuery) {
    "use strict";

    var w = kernel.global;
    w._googleApiLoadCallback = function() {
        w.GMAPS_API_AVAILABLE = true;
        topic.publish("/google/maps/apiloaded", w.google.maps);
    };

    return {
        load: function(gmapsUri, gmapsParams, loadCallback) {
            if(w.GMAPS_API_AVAILABLE) {
                return w.google.maps;
            } else {
                topic.subscribe("/google/maps/apiloaded", loadCallback);

                var query = ioQuery.objectToQuery(gmapsParams);
                var uri = gmapsUri + '?' + (query.length ? query + '&' : '') + 'callback=_googleApiLoadCallback';

                require([uri], function() { /* Do nothing */ });
            }
        }
    };

});