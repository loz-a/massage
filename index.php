<?php
    error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));

    $pos = strpos($_SERVER['REQUEST_URI'], 'image.php?');
    if (false !== $pos) {
        include 'image.php';
        exit;
    }


    function redirect($status, $location) {
        header('HTTP/1.1 ' . $status);
        header('Location: ' . $location);
        exit();
    } // redirect()


    // Час останнього редагування сторінки,
    $lastModifiedTimestamp = 1424104840926;

    $ifModifiedSince = false;
    if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
        $ifModifiedSince = strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']);
    }

    if ($ifModifiedSince and $ifModifiedSince >= $lastModifiedTimestamp) {
        header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
        exit();
    }

    header("Last-Modified: " . gmdate("D, d M Y H:i:s", $lastModifiedTimestamp) . " GMT");

    $defaultLanguage       = 'ru';
    $defaultPage           = 'index.html';
    $requiredPageExtension = 'html';
    $isRedirect            = false;
    $uri                   = $_SERVER['REQUEST_URI'];

    if (!empty($_SERVER['QUERY_STRING'])) {
        $uri = substr($uri, 0, strpos($uri, '?'));
    }
    $explodedUri = array_slice(explode('/', $uri), 1);
    // clear array from empty values
    $explodedUri = array_filter($explodedUri, function($el) { return !empty($el); });

    $lang = ''; $page = '';

    switch(count($explodedUri)) {
        case 0:
            $lang = $defaultLanguage;
            $page = $defaultPage;
            $isRedirect = true;
            break;
        case 1:
            if (strrpos($explodedUri[0], $requiredPageExtension)
                + strlen($requiredPageExtension) === strlen($explodedUri[0])
            ) {
                // if ends with .html
                $lang = $defaultLanguage;
                $page = $explodedUri[0];
            }
            else {
                $lang = $explodedUri[0];
                $page = $defaultPage;
            }
            $isRedirect = true;
            break;
        default:
            $lang = $explodedUri[0];
            $page = $explodedUri[1];
    }

    if ($isRedirect) {
        redirect('301 Moved Permanently', sprintf('http://%s/%s/%s', $_SERVER['HTTP_HOST'], $lang, $page));
    }

    $page = substr($page, 0, strlen($page) - strlen($requiredPageExtension) - 1);
    $includeFile = sprintf('pages/%s/%s.phtml', $lang, $page);
    if (!file_exists($includeFile)) {
        redirect('404 Not Found', sprintf('http://%s/%s/%s', $_SERVER['HTTP_HOST'], $lang, '404.html'));
    }

    ob_start();
    include $includeFile;
    $content = ob_get_contents();
    ob_end_clean();


    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
        and strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    ) {
        /* special ajax here */
        echo $content;
        exit();
    }

?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?= $lang === 'uk' ? 'Массаж у Моршині' : 'Массаж в Моршине' ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Loz Andriy">

    <!-- css -->
    <link rel="stylesheet" href="/css/main.css">
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/css/ie.css"/>
    <![endif]-->

    <!-- js -->
    <script>
        document.cookie = "device_dimensions=" + screen.width + "x" + screen.height;
    </script>
    <script src="/scripts/vendor/modernizr-2.6.2.js"></script>

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="/img/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/img/ico/apple-touch-icon-144.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/ico/apple-touch-icon-114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/ico/apple-touch-icon-72.png">
    <link rel="apple-touch-icon-precomposed" href="/img/ico/apple-touch-icon-57.png">

    <!-- Authors -->
    <link rel="author" href="/humans.txt" />
</head>
<body>

    <div id="main" class="main-container">

        <header class="clearfix animated fadeIn">
        <? include realpath(__DIR__)."/pages/$lang/header.phtml" ?>
        </header>
        <? include realpath(__DIR__)."/pages/$lang/navigation.phtml" ?>

        <?= $content; ?>

    </div>

    <script>
        var dojoConfig = {
            async: 1,
            paths: {
                "navigation": "../../navigation",
                "location": "../../location",
                "google-maps-loader": "../../google-maps-loader",
                "put-selector": "../../vendor/put-selector/put",
                'DomStorage': '../../DomStorage',
                'Carousel': '../../Carousel',
                'view-photos': '../../view-photos',
                'lazy-animation': '../../lazy-animation'
            },
            locale: '<?= $lang ?>'
        };

    </script>
    <script src="/scripts/vendor/dojo/dojo.js"></script>
    <script src="/scripts/main.js"></script>
</body>
</html>