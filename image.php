<?php

    $device_width  = 0;
    $device_height = 0;
    $file = $_SERVER['QUERY_STRING'];

    if (!file_exists($file)) return;

    // read the device viewport dimensions
    if (!isset($_COOKIE['device_dimensions'])) return;
    $dimensions = explode('x', $_COOKIE['device_dimensions']);
    if (count($dimensions) != 2) return;

    $device_width  = (int) $dimensions[0];
    $device_height = (int) $dimensions[1];

    // load image for phones and tablets. If user change device orientation
    $image_width = $device_width > $device_height ? $device_width : $device_height;

    if ($image_width <= 0) return;

    $fileext = pathinfo($file, PATHINFO_EXTENSION);
    $output_file = null;

    switch(true) {

        // Low resolution image
        case ($image_width <= 800):
            $output_file = substr_replace($file, '-low', -strlen($fileext) - 1, 0);
            break;

        // medium resolution image
        case ($image_width <= 1024):
            $output_file = substr_replace($file, '-med', -strlen($fileext) - 1, 0);
            break;
    }

    // check the file exists
    if ($output_file and file_exists($output_file)) {
        $file = $output_file;
    }

    $mime = get_content_type($fileext);

    header("Content-Type: $mime");
    readfile($file);



    function get_content_type($fileext)
    {
        $mime = '';

        switch(strtolower($fileext)) {
            case 'gif':
                $mime = image_type_to_mime_type(IMAGETYPE_GIF);
                break;
            case 'png':
                $mime = image_type_to_mime_type(IMAGETYPE_PNG);
                break;
            case 'jpg':
            case 'jpeg':
                $mime = image_type_to_mime_type(IMAGETYPE_JPEG);
                break;
        }
        return $mime;
    } // get_content_type()